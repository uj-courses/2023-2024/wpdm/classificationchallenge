from sklearn.dummy import DummyClassifier

class PipelineWrapper:
    def __init__(self):
        self.dummy = DummyClassifier(strategy='most_frequent', random_state=42)

    def fit(self,X,y):
        """

        :param X: pandas Dataframe or numpy array with features, not including target variable
        :param y: np.array containing target variable
        """
        self.dummy.fit(X, y)


    def predict(self,X):
        """

        :param X: pandas Dataframe or numpy array with features, not including target variable
        :return:  np.array with predictions for X
        """

        return self.dummy.predict(X)
